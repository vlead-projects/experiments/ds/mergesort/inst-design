#+TITLE: MergeSort - Index of Artefacts from Instructional Designer 
#+AUTHOR: Priya Raman 
#+DATE: [2018-05-08 Tue]
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This is an index to all documents and artefacts shared by
  the Instructional Designer ( Zia) for the merge sort experiment.

* Link to Artefacts 
  
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  | S. No | Date             | Purpose                           | link                     | Remarks            | Sent by |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    1. | [2018-04-23 Mon] | First Draft of the design doc     | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpMnozVDBxR2tZcDdkakdyaTVVWWFrdEhLbHNj][Design Doc]]               |                    | Zia     |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    2. | [2018-04-23 Mon] | Content gap for pre test,         | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpQkJ3YzRNdVo5R1VZeDFISFRfRi1wSW51QXRB][content gap, mod 1 and 2]] |                    | Zia     |
  |       |                  | module 1 and module 2             |                          |                    |         |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    3. | [2018-04-26 Thu] | Content gap for module 3          | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpa2dDc1ZLQXZjOHFJcURNaWVTeGU0bklWcVgw][content gap mod 3 & 4]]    |                    | Zia     |
  |       |                  | and module 4                      |                          |                    |         |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    4. | [2018-04-26 Thu] | Content requirement doc           | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpQm5XZTA4RUJ0SlZNdEJ5alQ3Q1Bzb2hwWTA4][content requirement]]      |                    | Zia     |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    5. | [2018-05-05 Sat] | Content requirement for post      | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpa2dDc1ZLQXZjOHFJcURNaWVTeGU0bklWcVgw][content gap mod 3 & 4]]    |                    |         |
  |       |                  | assessment                        |                          |                    |         |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    6. | [2018-05-08 Tue] | Phase 1 - Story Board - Version 1 | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpa3BRWWNvS1IzbW5sMEJqemJhNFJlZE92RVNz][Ph - 1 story board]]       | First Draft        | Zia     |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    7. | [2018-05-10 Thu] | Phase 1 - Story Board - Version 2 | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpUXpuVDBwbXRUWkpvTHpTdXpMQXVNbmZYRWdJ][Ph1 - story board ver 2]]  | 2nd version        | Zia     |
  |       |                  |                                   |                          | with MK's comments |         |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    8. | [2018-05-15 Tue] | Phase 1 - Story Board - Version 3 | Ph1 - [[https://drive.google.com/open?id=0B5QyKA0Uc9mpVF9qaG1oQ2k1dFhiR25ZWS11TFFVSGk3SWY0][story board ver 3]]  | with MK's feedback | Zia     |
  |       |                  |                                   |                          | Incorporated       |         |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |    9. | [2018-05-15 Tue] | Phase 2 - Story Board - Version 1 | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpQnZvb3BiRmlyS0U5ZmJkUFJtU3JOLXpUTzlB][Ph - 2 story board]]       | First Draft        | Zia     |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |   10. | [2018-05-28 Mon] | Story Board - Final Version 1     | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpbXhxcDZzV0hNNXdWZ001VDJpWWxONFlURG5J][Complete story board]]     | First Draft        | Zia     |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
  |   11. | [2018-05-28 Mon] | Exp Blue Print listing            | [[https://drive.google.com/open?id=0B5QyKA0Uc9mpQVFucnRqWUNLTkk1VGJjbFpUUU1mUDJNY1pv][Exp Blueprint listing]]    | First Draft        | Zia     |
  |-------+------------------+-----------------------------------+--------------------------+--------------------+---------|
